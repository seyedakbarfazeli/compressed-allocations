import * as path from 'path'

export const fromProject = (project: string) => {
  const IMPORT_RAW_CSV_NAME = 'raw.csv';
  const EXPORT_RAW_ADVERTISED_FOR_CSV_NAME = 'raw-advertised-for.csv';
  const EXPORT_RAW_IGNORED_CSV_NAME = 'raw-ignored.csv';
  const EXPORT_RAW_NOT_IGNORED_CSV_NAME = 'raw-not-ignored.csv';
  const EXPORT_CREDITS_CSV_NAME = 'credits.csv';
  const EXPORT_PULSEX_DISTRIBUTION_CSV_NAME = 'pulsex-distribution.csv';
  const EXPORT_POINT_EVENT_NAME = 'point-event.csv';
  const EXPORT_RAW_REDUCED_NAME = 'raw-reduced.csv';
  const EXPORT_RAW_SALES_CSV_NAME = 'sales.csv';

  const DATA_DIR = './data';
  return {
    IMPORT_RAW_CSV_NAME,
    EXPORT_RAW_ADVERTISED_FOR_CSV_NAME,
    EXPORT_RAW_IGNORED_CSV_NAME,
    EXPORT_RAW_NOT_IGNORED_CSV_NAME,
    EXPORT_CREDITS_CSV_NAME,
    EXPORT_PULSEX_DISTRIBUTION_CSV_NAME,
    EXPORT_POINT_EVENT_NAME,
    EXPORT_RAW_REDUCED_NAME,
    EXPORT_RAW_SALES_CSV_NAME,
    DATA_DIR,
    RAW_CSV_PATH: path.join(DATA_DIR, project, IMPORT_RAW_CSV_NAME),
    RAW_ADVERTISED_FOR_CSV_PATH: path.join(DATA_DIR, project, EXPORT_RAW_ADVERTISED_FOR_CSV_NAME),
    RAW_NOT_IGNORED_CSV_PATH: path.join(DATA_DIR, project, EXPORT_RAW_NOT_IGNORED_CSV_NAME),
    RAW_IGNORED_CSV_PATH: path.join(DATA_DIR, project, EXPORT_RAW_IGNORED_CSV_NAME),
    RAW_SALES_CSV_PATH: path.join(DATA_DIR, project, EXPORT_RAW_SALES_CSV_NAME),
    CREDITS_CSV_PATH: path.join(DATA_DIR, project, EXPORT_CREDITS_CSV_NAME),
    PULSEX_DISTRIBUTION_CSV_PATH: path.join(DATA_DIR, project, EXPORT_PULSEX_DISTRIBUTION_CSV_NAME),
    POINT_EVENT_PATH: path.join(DATA_DIR, project, EXPORT_POINT_EVENT_NAME),
    RAW_REDUCED_CSV_PATH: path.join(DATA_DIR, project, EXPORT_RAW_REDUCED_NAME),
  }
}
